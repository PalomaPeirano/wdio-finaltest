import HomePage from '../pages/homePage.js';
import CataloguePage from '../pages/CataloguePage.js';
import CompareData from '../processes/compareData.js'

let category = 'Women';

describe(`Find all expected products under the category ${category}`, async () => {
    let womanArticlesNames;
    let topsArticlesNames;
    let dressesArticlesNames;
    it(`It should retrieve all articles names`, async () => {
        await HomePage.abrir('/');
        await HomePage.clickearElemento(await HomePage.womanCategory);
        womanArticlesNames = await CataloguePage.getArticlesNames();
    });
    it(`It should retrieve the names of all articles in tops subcategory`, async () => {
        await HomePage.goSubCategoryInWoman('Tops');
        topsArticlesNames = await CataloguePage.getArticlesNames();
    });
    it(`It should retrieve the names of all articles in dresses subcategory`, async () => {
        await HomePage.goSubCategoryInWoman('Dresses');
        dressesArticlesNames = await CataloguePage.getArticlesNames();
    });
    it(`All articles found under the sub-categories of ${category} should be found under ${category} category`, async () => {
        await expect(await CompareData.deepCompare(womanArticlesNames, topsArticlesNames, dressesArticlesNames)).to.be.true;
    })

});
