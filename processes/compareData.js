class DataComparision {

    /**
     * Compare data of the first parameter against the following
     * Allows products that are not found under any women's sub-category, being found under women category. Ex: pants
     * More subcategories could be passed as arguments
    */
    static async compareData() {
        let args = Array.from(arguments);
        let womanArticles = args[0];
        let subcategoryArticles = args.splice(0, 1)
        let result;
        subcategoryArticles.forEach(element => {
            result = element.every(article => womanArticles.includes(article))
        });
        return result;
    }

    /**
     * Compare data of the first parameter against the following
     * Under women category should not be products that are not under any of its subcategories.
     * More subcategories could be passed as arguments
    */
    static async deepCompare() {
        let args = Array.from(arguments);
        let womanArticles = args[0];
        let subcategoryArticles = [];
        for (let i = 1; i < args.length; i++) {
            subcategoryArticles = subcategoryArticles.concat(args[i]);
        }
        return JSON.stringify(womanArticles) == JSON.stringify(subcategoryArticles);
    }

}
export default DataComparision;
