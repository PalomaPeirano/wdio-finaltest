class FilterData {

    /**
     * Filter data in articlesNames according to the type of products expected in typeOfProduct
     * The name of the product should include any of the words [type] of the category.
     * Ex: The name of a top sould include the word 't-shirt' or the word 'blouse'
    */
    async filterData(articlesNames, typeOfProduct) {
        return await Promise.all(articlesNames.map(async name => await name.getText()))
            .then(names => {
                return names.filter(name => typeOfProduct.some(type => name.toLowerCase().includes(type)))
            }).catch((err) => console.log(err))
    }

}
export default new FilterData().filterData;