import BasePage from '../pages/basePage.js';

class CataloguePage extends BasePage {

    // WebElements
    get articlesNames() { return $$(`div.right-block a.product-name`) };

    //-------------------------------------------------------------------------------------------------------//
    async getArticlesNames () {
        const articlesNames = await this.articlesNames;
        return Promise.all(articlesNames.map(async name => await name.getText()))
            .then(names => {return names})
            .catch((err) => console.log(err));
    }
}
export default new CataloguePage();
